#include "../src/infrastructure/persistence/user_repository.hpp"
#include "../src/interface/cli/user_cli.hpp"
#include "../src/core/use_cases/use_cases.hpp"

int main() {
    InMemoryUserRepository userRepository;

    CreateUserUseCase createUserUseCase(userRepository);
    ReadUserUseCase readUserUseCase(userRepository);
    UpdateUserUseCase updateUserUseCase(userRepository);
    DeleteUserUseCase deleteUserUseCase(userRepository);

    UserCLI userCLI(createUserUseCase, readUserUseCase, updateUserUseCase, deleteUserUseCase);
    userCLI.run();

    return 0;
}