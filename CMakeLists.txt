cmake_minimum_required(VERSION 3.15)
project(user_management)

set(CMAKE_CXX_STANDARD 17)

FILE(GLOB_RECURSE PROJECT_SRC src/*.cpp)

add_executable(user_management
    app/main.cpp
    ${PROJECT_SRC}
)

target_include_directories(user_management PRIVATE
    src/*
)