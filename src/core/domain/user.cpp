#include "user.hpp"

User::User(const std::string& name, int age)
    : m_name(name), m_age(age) {}

std::string User::getName() const {
    return m_name;
}

int User::getAge() const {
    return m_age;
}

void User::setName(const std::string& name) {
    m_name = name;
}

void User::setAge(int age) {
    m_age = age;
}