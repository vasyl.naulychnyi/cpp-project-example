#pragma once

#include <string>

class User {
public:
    User(const std::string& name, int age);

    std::string getName() const;
    int getAge() const;

    void setName(const std::string& name);
    void setAge(int age);

private:
    std::string m_name;
    int m_age;
};
