
#include "../../infrastructure/persistence/user_repository.hpp"
#include "../domain/user.hpp"
#include "use_cases.hpp"

ReadUserUseCase::ReadUserUseCase(UserRepositoryInterface& userRepository)
    : m_userRepository(userRepository) {}

User ReadUserUseCase::execute(int id) {
    User* user = m_userRepository.readUser(id);
    if (user) {
        return *user;
    }
    return User(0, 0);
}
