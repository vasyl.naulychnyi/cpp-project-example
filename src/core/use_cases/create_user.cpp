
#include "../domain/user.hpp"
#include "use_cases.hpp"

CreateUserUseCase::CreateUserUseCase(UserRepositoryInterface& userRepository)
    : m_userRepository(userRepository) {}

void CreateUserUseCase::execute(const std::string& name, int age) {
    User user(name, age);
    m_userRepository.createUser(user);
}
