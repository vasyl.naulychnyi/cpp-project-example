
#include "../../infrastructure/persistence/user_repository.hpp"
#include "../domain/user.hpp"
#include "use_cases.hpp"

UpdateUserUseCase::UpdateUserUseCase(UserRepositoryInterface& userRepository)
    : m_userRepository(userRepository) {}

void UpdateUserUseCase::execute(int id, const std::string& name, int age) {
    User user(name, age);
    m_userRepository.updateUser(id, user);
}
