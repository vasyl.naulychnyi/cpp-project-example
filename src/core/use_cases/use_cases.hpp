#pragma once

#include "../../interface/persistence/user_repository_interface.hpp"
#include "../domain/user.hpp"

class CreateUserUseCase {
   public:
    CreateUserUseCase(UserRepositoryInterface& userRepository);

    void execute(const std::string& name, int age);

   private:
    UserRepositoryInterface& m_userRepository;
};

class ReadUserUseCase {
   public:
    ReadUserUseCase(UserRepositoryInterface& userRepository);

    User execute(int id);

   private:
    UserRepositoryInterface& m_userRepository;
};

class UpdateUserUseCase {
   public:
    UpdateUserUseCase(UserRepositoryInterface& userRepository);

    void execute(int id, const std::string& name, int age);

   private:
    UserRepositoryInterface& m_userRepository;
};

class DeleteUserUseCase {
   public:
    DeleteUserUseCase(UserRepositoryInterface& userRepository);
    void execute(int id);

   private:
    UserRepositoryInterface& m_userRepository;
};
