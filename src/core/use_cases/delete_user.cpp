
#include "../../infrastructure/persistence/user_repository.hpp"
#include "use_cases.hpp"

DeleteUserUseCase::DeleteUserUseCase(UserRepositoryInterface& userRepository)
    : m_userRepository(userRepository) {}

void DeleteUserUseCase::execute(int id) { m_userRepository.deleteUser(id); }
