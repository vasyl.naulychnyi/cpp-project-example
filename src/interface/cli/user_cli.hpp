#pragma once

#include "../../core/use_cases/use_cases.hpp"

class UserCLI {
public:
    UserCLI(CreateUserUseCase& createUserUseCase,
            ReadUserUseCase& readUserUseCase,
            UpdateUserUseCase& updateUserUseCase,
            DeleteUserUseCase& deleteUserUseCase);

    void run();

private:
    CreateUserUseCase& m_createUserUseCase;
    ReadUserUseCase& m_readUserUseCase;
    UpdateUserUseCase& m_updateUserUseCase;
    DeleteUserUseCase& m_deleteUserUseCase;
};