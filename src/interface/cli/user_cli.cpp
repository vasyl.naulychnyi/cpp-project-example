#include "user_cli.hpp"

#include <iostream>
#include <string>

UserCLI::UserCLI(CreateUserUseCase &createUserUseCase,
                 ReadUserUseCase &readUserUseCase,
                 UpdateUserUseCase &updateUserUseCase,
                 DeleteUserUseCase &deleteUserUseCase)
    : m_createUserUseCase(createUserUseCase),
      m_readUserUseCase(readUserUseCase),
      m_updateUserUseCase(updateUserUseCase),
      m_deleteUserUseCase(deleteUserUseCase) {}

void UserCLI::run() {
    int choice;
    do {
        std::cout << "User Management Menu:" << std::endl;
        std::cout << "1. Create User" << std::endl;
        std::cout << "2. Read User" << std::endl;
        std::cout << "3. Update User" << std::endl;
        std::cout << "4. Delete User" << std::endl;
        std::cout << "5. Exit" << std::endl;
        std::cout << "Enter your choice: ";
        std::cin >> choice;

        switch (choice) {
            case 1: {
                std::string name;
                int age;
                std::cout << "Enter name: ";
                std::cin >> name;
                std::cout << "Enter age: ";
                std::cin >> age;
                m_createUserUseCase.execute(name, age);
                break;
            }
            case 2: {
                int id;
                std::cout << "Enter user ID: ";
                std::cin >> id;
                User user = m_readUserUseCase.execute(id);
                if (user.getName().empty()) {
                    std::cout << "User not found." << std::endl;
                } else {
                    std::cout << "Name: " << user.getName()
                              << ", Age: " << user.getAge() << std::endl;
                }
                break;
            }
            case 3: {
                int id;
                std::string name;
                int age;
                std::cout << "Enter user ID: ";
                std::cin >> id;
                std::cout << "Enter new name: ";
                std::cin >> name;
                std::cout << "Enter new age: ";
                std::cin >> age;
                m_updateUserUseCase.execute(id, name, age);
                break;
            }
            case 4: {
                int id;
                std::cout << "Enter user ID: ";
                std::cin >> id;
                m_deleteUserUseCase.execute(id);
                break;
            }
            case 5:
                std::cout << "Exiting..." << std::endl;
                break;
            default:
                std::cout << "Invalid choice. Please try again." << std::endl;
                break;
        }
        std::cout << std::endl;
    } while (choice != 5);
}
