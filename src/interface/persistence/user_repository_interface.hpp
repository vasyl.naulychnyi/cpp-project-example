#pragma once

#include "../../core/domain/user.hpp"

class UserRepositoryInterface {
public:
    virtual ~UserRepositoryInterface() = default;
    virtual void createUser(const User& user) = 0;
    virtual User* readUser(int id) = 0;
    virtual void updateUser(int id, const User& user) = 0;
    virtual void deleteUser(int id) = 0;
};
