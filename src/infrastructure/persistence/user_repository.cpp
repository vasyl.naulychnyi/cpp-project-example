#include "user_repository.hpp"

void InMemoryUserRepository::createUser(const User& user) {
    m_users.push_back(user);
}

User* InMemoryUserRepository::readUser(int id) {
    if (id >= 0 && id < static_cast<int>(m_users.size())) {
        return &(m_users[id]);
    }
    return nullptr;
}

void InMemoryUserRepository::updateUser(int id, const User& user) {
    if (id >= 0 && id < static_cast<int>(m_users.size())) {
        m_users[id] = user;
    }
}

void InMemoryUserRepository::deleteUser(int id) {
    if (id >= 0 && id < static_cast<int>(m_users.size())) {
        m_users.erase(m_users.begin() + id);
    }
}
