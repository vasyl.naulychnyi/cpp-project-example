#pragma once

#include <optional>
#include <vector>

#include "../../interface/persistence/user_repository_interface.hpp"

class InMemoryUserRepository : public UserRepositoryInterface {
   public:
    void createUser(const User& user) override;
    User* readUser(int id) override;
    void updateUser(int id, const User& user) override;
    void deleteUser(int id) override;

   private:
    std::vector<User> m_users;
};
