# User Management Console Application

This is a simple console application written in C++ that demonstrates Object-Oriented Programming (OOP) principles and Clean Architecture. The application provides CRUD (Create, Read, Update, Delete) operations for managing users.

## Features

- Create a new user with a name and age
- Read user details by ID
- Update a user's name and age by ID
- Delete a user by ID

## Project Structure

The project follows the Clean Architecture pattern, separating concerns into different layers:

- **Domain Layer**: Contains the core business logic and entities (e.g., `User` class).
- **Use Case Layer**: Defines the application-specific operations (e.g., CRUD operations for `User`).
- **Infrastructure Layer**: Handles the implementation details (e.g., in-memory persistence with `UserRepository`).
- **Interface Layer**: Provides the user interface (Command-Line Interface in this case).


## Building and Running

This project uses CMake as the build system. To build and run the application, follow these steps:

1. Clone the repository:
    ```sh
    git clone <repo link here>
    ```
2. Navigate to the project directory:
    ```sh
    cd cpp-project-example
    ```
3. Create a `build` directory and navigate to it:
    ```sh
    mkdir build
    cd build
    ```
4. Generate the build files using CMake:
    ```
    cmake ..
    ```
5. Build the project:
    ```
    cmake --build .
    ```
6. Run the executable:
    ```
    ./user_management
    ```

You should now see the User Management Menu in the console, where you can perform CRUD operations on users.

## Dependencies

This project has no external dependencies. It uses the standard C++ library and follows the C++17 standard.

